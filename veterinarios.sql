﻿-- Ejemplo de creación de una base de datos de veterinarios
-- Tiene 3 tablas
DROP DATABASE IF EXISTS b20190528;
CREATE DATABASE b20190528;
USE b20190528;

/*
  creando la tabla animales

*/


CREATE TABLE animales(
id int AUTO_INCREMENT,
nombre varchar(100),
raza varchar(100),
fechaNac date,    
PRIMARY KEY(id) -- creando la clave
);
CREATE TABLE veterinarios(
  cod int AUTO_INCREMENT,
  nombre varchar(100),
  especialid varchar(100),
  PRIMARY KEY(cod)
 );
CREATE TABLE acuden(
  idanimal int,
  cod_Vet int,
  fecha date,
  PRIMARY KEY(idanimal,cod_Vet,fecha),
  UNIQUE KEY(idanimal),
  CONSTRAINT fkacudenanimal FOREIGN KEY(idanimal)
  REFERENCES animales(id),
  CONSTRAINT fkacudenveterinarios FOREIGN KEY(cod_Vet)
  REFERENCES veterinarios(cod)
 );
-- insertar un registro

  INSERT INTO animales (nombre, raza, fechaNac)
  VALUES ('jorge','bulldog','2000/2/1'),
         ('ana','caniche','2002/1/2');
  
  -- listar
    
    SELECT * FROM animales AS a;
      



